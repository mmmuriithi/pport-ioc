<?php
namespace pPort\Ioc;
class Accessor
{	
	public $instantiate=true;
	public $alias;
	public static function __callStatic($method, $args) 
	{
		$class_instance=static::fetchInstance($method,$args);

		return call_user_func_array(array($class_instance, $method), $args);
	}

	public function __call($method, $args) 
	{
		return call_user_func_array(array(static::fetchInstance($method,$args), $method), $args);
	}


	public static function fetchInstance($method,$args)
	{
		$accessor=new static();
		$service=$accessor->register();
		$alias=$accessor->alias;
		if($accessor->instantiate)
		{
			return (is_object($service))?$service:new $service();
		}
		else
		{
			return $service;
		}
		
	}
}