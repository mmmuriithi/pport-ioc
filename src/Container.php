<?php
namespace pPort\Ioc;

/**
 * Simple Dependency Injection Container & Application Components Manager
 * Packages/Services : Items within the Container
 */


class Container implements \ArrayAccess
{

	/**
	* @var array
	*/
	public $resolvers=[];

	/**
	* @var array
	*/
	public $aliases=[];

	/**
	* @var array
	*/
	public $params=[];
	/**
	* @var array
	*/

	/**
	* @var array
	*/
	public $packages=[];

	/**
	* @var array
	*/
	protected $instances=[];

	/**
	* @var string
	*/
	public $config_path='packages.php';

	
	


	public function __construct($params=[])
	{
		$this->boot($params)->register($this->packages);
	}

	public function boot($params=[])
	{
		if(isset($params['config_path'])&&file_exists($params['config_path']))
		{
			$this->config_path=$params['config_path'];
			$this->set_packages();
		}
		
		return $this;
	}

	//Locate services/packages
	public function set_packages($packages=[])
	{
		$this->packages=!empty($packages)?$packages:include($this->packages_config_path);
	}

	public function set_alias($resolver,$alias,$autoload=false)
	{
		if(!class_exists($alias) && !in_array($alias,$this->aliases))
		{
			class_alias($resolver,$alias,$autoload);	
		}
	}

	public function get_services()
	{
		return $this->resolvers;
	}

	public function get_aliases()
	{
		return $this->aliases;
	}

	

	



	//Register the service
	public function register($handle,$resolver=false,$alias=false,$autoload=false)
	{
		if(is_array($handle))
		{
			foreach($handle as $service_key=>$actual_service)
			{
				if(is_array($actual_service))
				{
					$alias=isset($actual_service[1])?$actual_service[1]:$service_key;
					$autoload=isset($actual_service[2])?$actual_service[2]:false;
					$actual_service=$actual_service[0];
				}
				//Check if Resolver is instance of pPort/Provider, if yes provide its services and don't register the provider
				$this->register($service_key,$actual_service,$alias,$autoload);
				
			}
		}
		else
		{

			if(!array_key_exists($handle,$this->resolvers))
			{

				if($resolver)
				{
					//Disable check for existence of a class during registration
					/*if((class_exists($resolver)||is_object($resolver)))
					{*/
						$alias=($alias)?$alias:$handle;
						if(is_subclass_of($resolver, '\\pPort\\Ioc\\Provider'))
						{
						    $provider=new $resolver();
							$provider->provide($this);
						}
						else
						{
							if(is_subclass_of($resolver, '\\pPort\\Ioc\\Accessor'))
							{
								$this->set_alias($resolver,$alias);
								$this->aliases[$handle]=$alias;	
							}

							$this->resolvers[$handle]=$resolver;
						}
					/*}*/
						
				}
				else
				{
					throw new \Exception('Kindly specify how your registered service should be resolved.');
				}
				
			}
		}
		return $this;
	}


	
	
	/**
	 * @param      $abstract
	 * @param null $concrete
	 */
  	public function has($abstract)
	{
		if(array_key_exists($abstract,$this->resolvers)||($abstract=array_search($abstract, $this->resolvers)))
		{
			return true;
		}
		return false;
	}

	//Can Register anonymous functions 
	public function __set($abstract,$concrete)
	{
		$this->register($abstract,$concrete);
	}

	//Load a service in container
	public function __call($abstract, $parameters) {

	    return $this->get($abstract,$args);	  
	}

	//Load a service in container
	public function __get($abstract) 
	{
		return $this->get($abstract);
  	}


  	public function offsetExists ($abstract)
    {
        return array_key_exists($abstract, $this->resolvers);
    }

    public function offsetGet ($abstract)
    {
       return $this->get($abstract);    
    }

    public function offsetSet ($abstract, $concrete)
    {
        return $this->register($abstract,$concrete);    
    }

    public function offsetUnset ($abstract)
    {
        unset($this->resolvers[$abstract]);
        return $this;
    }

    //Allow registration of hooks
    public function modify($abstract,$concrete)
    {
    	return $concrete($this[$abstract],$this);
    	
    }

  	

	/**
	 * @param      $abstract
	 * @param null $concrete
	 */
	public function set($abstract, $concrete = NULL)
	{
		if ($concrete === NULL) {
			$concrete = $abstract;
		}
		$this->register($abstract,$concrete);
	}


  	/**
	 * @param       $abstract
	 * @param array $parameters
	 *
	 * @return mixed|null|object
	 * @throws Exception
	 */
	public function get($abstract,$params=array(),$new=false)
	{

		$alias_key=array_search($abstract,$this->aliases)?:$abstract;
		
		if(!isset($this->resolvers[$alias_key]))
		{

			$this->set($abstract);
		}

		if(!isset($this->instances[$alias_key])||$new)
		{

			$instance=$this->resolve($this->resolvers[$alias_key], $params);
			$this->instances[$alias_key]=$instance;
		}
		else
		{
			$instance=$this->instances[$alias_key];
		}
				
		return $instance;
	}


	/**
	 * resolve single
	 *
	 * @param $concrete
	 * @param $parameters
	 *
	 * @return mixed|object
	 * @throws Exception
	 */
	public function resolve($concrete, $parameters)
	{
		if ($concrete instanceof \Closure) {
			return $concrete($this, $parameters);
		}
		$reflector = new \ReflectionClass($concrete);
		// check if class is instantiable
		if (!$reflector->isInstantiable()) {
			throw new \Exception("Class {$concrete} is not instantiable");
		}
		// get class constructor
		$constructor = $reflector->getConstructor();
		var_dump($constructor);
		if (is_null($constructor)) {
			// get new instance from class
			return $reflector->newInstance();
		}
		// get constructor params
		$parameters   = $constructor->getParameters();
		$dependencies = $this->get_dependencies($parameters);
		// get new instance with dependencies resolved
		return $reflector->newInstanceArgs($dependencies);
	}
	/**
	 * get all dependencies resolved
	 *
	 * @param $parameters
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_dependencies($parameters)
	{
		$dependencies = [];
		foreach ($parameters as $parameter) {
			// get the type hinted class
			$dependency = $parameter->getClass();
			if ($dependency === NULL) {
				// check if default value for a parameter is available
				if ($parameter->isDefaultValueAvailable()) {
					// get default value of parameter
					$dependencies[] = $parameter->getDefaultValue();
				} else {
					throw new \Exception("Can not resolve class dependency {$parameter->name}");
				}
			} else {
				// get dependency resolved
				$dependencies[] = $this->get($dependency->name);
			}
		}
		return $dependencies;
	}

	



  	

}
