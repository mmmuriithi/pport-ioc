# README #
pPort Service Manager Package : Easily enable management of application services, providers and easy access.

Installation
============

Install pport\ioc using composer : 

	composer require pport/ioc

Register A Service Provider
-----------
#### 1.	As a Closure ####

```php
<?php
	$ioc=new pPort\Ioc\Container();
	$student=function($c){
		$student_obj=new stdClass();
		$student_obj->first_name="Martin";
		return $student_obj; 
	};
	$ioc->register('student',$student);
	$ioc->student->first_name; //Will output Martin
;?>
```

#### 2.	As an Actual Class or Name-Space ####

```php
<?php
	//or if you have a  Student-Class, this will also work with name-spaced classes
	class Student
	{
		public $first_name;
		function __construct()
		{
			$this->first_name='Jones';
		}
		function get_first_name()
		{
			return $this->first_name;
		}
	}
	$ioc->register('student_class','Student');
	echo $ioc->student->first_name; //Will output Jones
;?>
```

#### 3.	Directly using get. ####

If a service is not registered, get automatically registers and returns the service-class
	
	$ioc->get('Student');

Get Registered Services
--------------------------------
#### 1.	Using the get method ####

		$ioc->get('student');

#### 2.	As a variable call ####

		$ioc->student;

#### 3.	As a function call ####

		$ioc->student();

#### 4.	As an array parameter ####

		$ioc['student'];

Recursive Resolution of Dependencies
--------------------------------
pPort Ioc supports recursive resolution of dependencies. The Student class below when loaded will have it's dependency auto-injected, the Teacher Class will also have it's dependency auto-injected :

```php
<?php
	class Lesson
	{
		public $lesson;
		function __construct()
		{
			$this->lesson='English';
		}
		function get_class()
		{
			return $this->lesson;
		}
	}


	class Teacher
	{
		public $class;
		public $lesson;
		function __construct(Lesson $lesson)
		{
			$this->lesson=$lesson;
			$this->class='1';
		}
		function get_class()
		{
			return $this->class;
		}
	}


	class Student
	{
		public $teacher;
		function __construct(Teacher $teacher)
		{
			$this->teacher=$teacher;
		}
		function get_teacher()
		{
			return $this->teacher;
		}
	}
	//The dependencies are recursively registered and injected to the student class when called
	$ioc['Student'];
;?>
```