<?php
	return [
'core'=>array('pPort\Core\Provider','Core',true),
	'mvc'=>array('pPort\Mvc\Provider','Mvc',true),
	'config'=>array('pPort\Config\Provider','Config',true),
	'helper'=>array('pPort\Helper\Provider'),
	'event'=>array('pPort\Event\Provider'),
	'url'=>array('pPort\Url\Provider','Url',true),
	'functions'=>array('pPort\Functions\Provider'),
];